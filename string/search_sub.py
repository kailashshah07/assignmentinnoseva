#3.	Write a program to loop through a list of words and check if the word contains a substring. If it does print the word.

class Ary(object):
    def search_sub(self,mainString,subString):
        #takes argument main string followed by sub string and returns 1 if found else return 0
        for w in mainString :
            if subString in w:
                return 1
        return 0


fruits = ["applebananamango","mellon","pineapple"]
sub_str = "mango"
a1 = Ary()
if a1.search_sub(fruits,sub_str) == 1:
    print ("The list contains the substring")
else:
    print ("The list dont contains the substring")

